# -*- coding: UTF-8 -*-

import datetime
import logging

import requests
from flask import current_app, redirect, request, jsonify, make_response

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


class FeedlyClientAuth(object):
    """An object that can be used to retrieve or refresh OAuth2 tokens for authentication.

    :param redirect_uri: A redirect URI string as specified by Feedly requirements.
    :param scope: A fixed string for scope as specified by Feedly requirements.
    :param client_id: The client_id string as registered with Feedly.
    :param client_secret: The client_secret string as registered with Feedly.
    """
    def __init__(self, redirect_uri, scope, client_id, client_secret):
        self.redirect_uri = redirect_uri
        self.scope = scope
        self.client_id = client_id
        self.client_secret = client_secret

    def authorize(self):
        """Make a call to Feedly authentication URL in order to redirect to the Feedly login form."""
        request_url = self._get_authentication_url()
        params = dict(
            response_type='code',
            client_id=self.client_id,
            redirect_uri=self._get_callback_url(),
            scope=self.scope
        )
        res = requests.post(url=request_url, params=params)
        return redirect(res.url)

    # def callback(self):
    #     """Callback function to retrieve 'code' parameter from URL."""
    #     if 'code' not in request.args:
    #         logger.error('CODE parameter not found in response')
    #         return None
    #     return request.args['code']

    def get_access_tokens(self, code):
        """Make a call to Feedly API to retrive access token, refresh token, and token expiry date.

        :param code: Code string returned from Feedly login form.
        :return:
        """
        if code is None:
            logger.error("could not retrieve access tokens: code was None")
            return None, None, None, None
        request_url = self._get_token_url()
        params = dict(
            code=code,
            client_id=self.client_id,
            client_secret=self.client_secret,
            grant_type='authorization_code',
            redirect_uri=self.redirect_uri,
            scope=self.scope
        )
        res = requests.post(url=request_url, params=params)
        data = res.json()

        id = data.get('id')
        refresh_token = data.get('refresh_token')
        access_token = data.get('access_token')
        #TODO: better handle error
        if access_token is None or refresh_token is None:
            logger.error("could not retrieve access tokens: " + data)
            return None, None, None, None

        now = datetime.datetime.now()
        expiry_date = now
        if data['expires_in']:
            expiry_date = now + datetime.timedelta(0, seconds=data['expires_in'])

        return id, access_token, refresh_token, expiry_date

    def refresh_access_token(self, refresh_token):
        """Refresh the access token after it has expired.

        :param refresh_token: Refresh token string stored from first call to get_access_tokens().
        :return:
        """
        if refresh_token is None:
            logger.error("refresh token was null")
            return None, None

        request_url = self._get_token_url()
        params = dict(
            refresh_token=refresh_token,
            client_id=self.client_id,
            client_secret=self.client_secret,
            grant_type='refresh_token'
        )
        res = requests.post(url=request_url, params=params)
        data = res.json()

        access_token = data.get('access_token')
        if access_token is None:
            logger.error("could not retrieve access tokens: " + data)
            return None, None

        now = datetime.datetime.now()
        expiry_date = now
        if data['expires_in']:
            expiry_date = now + datetime.timedelta(0, seconds=data['expires_in'])

        return access_token, expiry_date

    def _get_authentication_url(self):
        return _get_endpoint('v3/auth/auth')

    def _get_token_url(self):
        return _get_endpoint('v3/auth/token')


class FeedlyClient(object):
    """An object that can be used to retrieve data from Feedly using access_token.

    :param access_token: The access token string received after authenticating user.
    """
    def __init__(self, token):
        self.access_token = token

    def get_profile(self):
        """Get Feedly user profile information."""
        if self.access_token is None:
            return self._return_error('access token was null')
        request_url = _get_endpoint('v3/profile')
        headers = {
            "Authorization": "Bearer " + self.access_token,
            'Content-type': 'application/json'
        }
        res = requests.get(url=request_url, headers=headers)
        data = res.json()

        email = data.get('email')
        name = data.get('givenName')
        return email, name

    def get_subscriptions(self):
        """Get Feedly user subscriptions."""
        if self.access_token is None:
            return self._return_error('access token was null')
        request_url = _get_endpoint('v3/subscriptions')
        headers = {
            "Authorization": "Bearer " + self.access_token,
            'Content-type': 'application/json'
        }
        res = requests.get(url=request_url, headers=headers)
        data = res.json()
        return jsonify(data)

    def get_categories(self):
        """Get Feedly user categories."""
        if self.access_token is None:
            return self._return_error('access token was null')
        request_url = _get_endpoint('v3/categories')
        headers = {
            "Authorization": "Bearer " + self.access_token,
            'Content-type': 'application/json'
        }
        res = requests.get(url=request_url, headers=headers)
        data = res.json()
        return jsonify(data)

    def get_stream(self, stream_id, count, newerThan=None, continuation=None):
        """Get entry information from Feedly user stream(category).

        :param stream_id: Stream ID string to retrieve (usually comes from category).
        :param count: Count (max 10,000) of entries to retrieve.
        :param newerThan: Minimum date of article publication to retrieve.
        :param continuation: Continuation ID string if needed.
        :return:
        """
        if self.access_token is None:
            return self._return_error('access token was null')
        if count > 10000:
            count = 10000
        request_url = _get_endpoint('v3/streams/contents?streamId=' + stream_id)
        headers = {
            "Authorization": "Bearer " + self.access_token,
            'Content-type': 'application/json'
        }
        params = dict(
            count=count,
            ranked='newest',
            unreadOnly=False,
        )
        if newerThan is not None:
            params['newerThan'] = newerThan
        if continuation is not None:
            params['continuation'] = continuation

        res = requests.get(url=request_url, headers=headers, params=params)
        data = res.json()
        return jsonify(data)


def _get_endpoint(path=None):
    credentials = current_app.config['OAUTH_CREDENTIALS']['feedly']
    sandbox = credentials['sandbox']
    if sandbox is True:
        service_host = credentials['sandbox_url']
    else:
        service_host = credentials['prod_url']

    url = "https://%s" % service_host
    if path is not None:
        url += "/%s" % path
    return url


def _return_error(message):
    responseObject = {
        'message': message,
        'status': 'error'
    }
    return make_response(jsonify(responseObject)), 401
