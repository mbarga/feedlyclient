# -*- coding: UTF-8 -*-

import os


# flask app secret key
# SECRET_KEY = 'feedlyclient'
# SESSION_TYPE = 'filesystem'

# Feedly API
# public client id and secret keys for sandbox environment can be found here:
# https://groups.google.com/forum/#!forum/feedly-cloud
# NOTE: client secret is updated every couple of months for sandbox!
FEEDLY_CLIENT_ID = os.environ.get("FEEDLY_CLIENT_ID")  # "sandbox"
FEEDLY_CLIENT_SECRET = os.environ.get("FEEDLY_CLIENT_SECRET")  # expires on
FEEDLY_REDIRECT_URI = "http://localhost:8080"  # do not change this
FEEDLY_SANDBOX = True
FEEDLY_SCOPE = 'https://cloud.feedly.com/subscriptions'  # do not change this
OAUTH_CREDENTIALS = {
    'feedly': {
        'id': FEEDLY_CLIENT_ID,
        'secret': FEEDLY_CLIENT_SECRET,
        'redirect_uri': FEEDLY_REDIRECT_URI,
        'sandbox': FEEDLY_SANDBOX,
        'scope': FEEDLY_SCOPE,
        'sandbox_url': 'sandbox7.feedly.com',
        'prod_url': 'cloud.feedly.com'
    }
}

# Database (mongo)
MONGOALCHEMY_DATABASE = 'feeds'
MONGOALCHEMY_SERVER = 'localhost'
