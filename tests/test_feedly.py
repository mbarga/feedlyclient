# -*- coding: UTF-8 -*-

from unittest.mock import Mock, patch
import datetime

from flask import Flask

from wrapper.feedly import FeedlyClientAuth, FeedlyClient


class TestFeedly(object):

    def setup(self):
        self.app = self._create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client(use_cookies=True)

    def teardown(self):
        self.app_context.pop()

    def _create_app(self):
        app = Flask(__name__)
        # app.config['TESTING'] = True
        app.config.from_object('config')
        return app

    def _init_feedly(self):
        key = "mykey"
        secret = "mysecret"
        token = "mytoken"
        return FeedlyClient(key, secret, token)

    def test_feedlyclient_init(self):
        feedly = self._init_feedly()

        assert feedly.key is not None
        assert feedly.secret is not None
        assert feedly.access_token is not None

        assert feedly.sandbox == True
        assert feedly.service_host is not None

        path = "v3/test"
        assert feedly._get_endpoint(path) == "https://" + feedly.service_host + "/" + path

    @patch('wrapper.feedly.requests.get')
    def test_feedlyclient_get_profile(self, mock_get):
        feedly = self._init_feedly()

        profile_data = dict(
            email="myemail",
            givenName="givenName"
        )
        mock_get.return_value.ok = True
        mock_get.return_value.json.return_value = profile_data

        email, name = feedly.get_profile()
        assert email == "myemail"
        assert name == "givenName"

    def test_feedlyclientauth_init(self):
        oauth = FeedlyClientAuth()
        assert oauth.sandbox is not None
        assert oauth.service_host is not None
        assert oauth.redirect_uri is not None
        assert oauth.scope is not None
        assert oauth.client_id is not None
        assert oauth.client_secret is not None
        assert oauth.access_token is None
    #
    @patch('wrapper.feedly.requests.post')
    def test_feedlyclientauth_authorize_isok(self, mock_post):
        oauth = FeedlyClientAuth()
        mock_post.return_value.ok = True
        res = oauth.authorize()
        assert res is not None
        # TODO: test for presense of 'code' in callback request arguments

    # @patch('wrapper.feedly.requests.post')
    # def test_feedlyclientauth_authorize_isbad(self, mock_post):
    #     oauth = FeedlyClientAuth()
    #     mock_post.return_value.ok = False

    @patch('wrapper.feedly.request')
    def test_feedlyclientauth_callback_isok(self, mock_request):
        request_data = dict(
            args=dict(code="xldk2880")
        )
        mock_request.return_value.ok = True
        # mock_request.return_value.args['code'] = "xldk2899"
        mock_request.return_value.json.return_value = request_data

        oauth = FeedlyClientAuth()
        res = oauth.callback()
        assert res is not None

    @patch('wrapper.feedly.requests.post')
    def test_feedlyclientauth_get_access_tokens_isok(self, mock_post):
        callback_data = dict(
            id='myId',
            access_token='accessToken',
            refresh_token='refreshToken',
            expires_in=5000
        )
        mock_post.return_value.ok = True
        mock_post.return_value.json.return_value = callback_data

        oauth = FeedlyClientAuth()
        code = "mycode"
        id, access_token, refresh_token, expiry_date = oauth.get_access_tokens(code)
        assert id is not None
        assert access_token is not None
        assert refresh_token is not None
        assert isinstance(expiry_date, datetime.date)
