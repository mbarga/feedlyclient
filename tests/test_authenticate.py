# -*- coding: UTF-8 -*-

from flask import Flask
from wrapper import feedly


# https://docs.pytest.org/en/latest/unittest.html
# http://flask.pocoo.org/docs/0.12/testing/
class TestAuthCase(object):  # name must start with Test

    def setup(self):  # name must be setup
        # self.db = etc...
        # db.create_all()
        # Role.insert_roles()
        self.app = self._create_app()
        self.app_context = self.app.app_context()
        self.app_context.push()
        self.client = self.app.test_client(use_cookies=True)

    def teardown(self):  # name must be teardown
        # os.close(self.db_fd)
        # os.unlink(app.config['DATABASE'])
        # db.session.remove()
        # db.drop_all()
        self.app_context.pop()

    def _create_app(self):
        app = Flask(__name__)
        app.config['TESTING'] = True
        app.config.from_object('config')
        return app

    def test_oauthsignin_func(self):  # with name starting with test_ will get picked up
        assert self.app is not None

        oauth = feedly.FeedlyClientAuth()
        assert oauth.sandbox is True
