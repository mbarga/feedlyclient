#!/usr/bin/env python
# -*- coding: UTF-8 -*-

from authenticate import app


if __name__ == '__main__':
    # flask initialization
    app.run('0.0.0.0', port=8080)
