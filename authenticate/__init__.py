# -*- coding: UTF-8 -*-

from flask import Flask
# from flask_sqlalchemy import SQLAlchemy
from flask_mongoalchemy import MongoAlchemy
from flask_login import LoginManager
from flask_migrate import Migrate
from flask_session import Session


app = Flask(__name__, instance_relative_config=True)

# load config file
app.config.from_object('config')

# session initialization?
sess = Session()
sess.init_app(app)

# configures database for app
db = MongoAlchemy(app)
# db = SQLAlchemy(app)

# initialize migration
migrate = Migrate(app, db)

# use flask_login for user session management
lm = LoginManager(app)

# import views
# ! CIRCULAR DEPENDENCY: must come at the end !
from . import views
