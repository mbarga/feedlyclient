# -*- coding: UTF-8 -*-

import json
import logging
from datetime import datetime

from flask import current_app, request, redirect, render_template, url_for
from flask_login import logout_user, login_user

from wrapper.feedly import FeedlyClient, FeedlyClientAuth
from . import app
from .models import User, Category, Entry

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)


@app.route('/login', methods=['GET'])
def login():
    """Display the Feedly login form to authenticate user using OAuth2."""
    credentials = current_app.config['OAUTH_CREDENTIALS']['feedly']
    redirect_uri = credentials['redirect_uri']
    scope = credentials['scope']
    client_id = credentials['id']
    client_secret = credentials['secret']
    oauth = FeedlyClientAuth(redirect_uri, scope, client_id, client_secret)
    return oauth.authorize()


# NOTE: callback must be on root URL as in route below (https://developer.feedly.com/v3/sandbox/)
@app.route('/', methods=['GET'])
def oauth_callback():
    """Callback function after authenticating Feedly user through form."""
    # TODO: how best to handle when the root URL is hit without being a callback?
    if not request.args.get('code'):
        return render_template("index.html")
    code = request.args['code']

    # oauth2 authentication after callback
    credentials = current_app.config['OAUTH_CREDENTIALS']['feedly']
    redirect_uri = credentials['redirect_uri']
    scope = credentials['scope']
    client_id = credentials['id']
    client_secret = credentials['secret']
    oauth = FeedlyClientAuth(redirect_uri, scope, client_id, client_secret)

    try:
        id, access_token, refresh_token, token_expiry_date = oauth.get_access_tokens(code)
        client = FeedlyClient(access_token)
        email, nickname = client.get_profile()
        # TODO store access_token in session or cache?
        if id is None:
            # FIXME
            logger.error("ID was returned null from oauth.callback()")
            return redirect(url_for('index'))
        user = User.query.filter_by(feedly_id=id).first()
        if not user:
            user = User(
                feedly_id=id,
                email=email,
                access_token=access_token,
                refresh_token=refresh_token,
                token_expiry_date=token_expiry_date
            )
            user.save()
        # flask login for user object
        # login_user(user, True)
        return render_template("success.html")

    except TypeError as e:
        logger.error(e)
        return render_template("error.html")
    except TypeError as e:
        logger.error(e)
        return render_template("error.html")


# @app.route('/logout', methods=['GET'])
# def logout():
#     """Flask logout for user object"""
#     logout_user()
#     return redirect(url_for('index'))


@app.route('/subscriptions', methods=['GET'])
def subscriptions():
    """Get Feedly user subscriptions from API."""
    try:
        # TODO: remove fixed ID and encode within config file
        id = 'ee31f0cd-2a7e-462a-9c14-4bd3a50ebfa5'
        user = get_feedly_user(id)
        oauth = FeedlyClient(user.access_token)
        resp = oauth.get_subscriptions()
        return resp

    except TypeError as e:
        logger.error(e)
        return render_template("error.html")
    except TypeError as e:
        logger.error(e)
        return render_template("error.html")


@app.route('/categories', methods=['GET'])
def categories():
    """Get Feedly user categories from API."""
    try:
        # TODO: remove fixed ID and encode within config file
        id = 'ee31f0cd-2a7e-462a-9c14-4bd3a50ebfa5'
        user = get_feedly_user(id)
        oauth = FeedlyClient(user.access_token)
        resp = oauth.get_categories()
        data = json.loads(resp.data)
        for category in data:
            cat = Category.query.filter_by(id=category.get('id')).first()
            if not cat:
                cat = Category(
                    id=category.get('id'),
                    label=category.get('label')
                )
            else:
                cat.label = category.get('label')
            cat.save()
        return resp

    except TypeError as e:
        logger.error(e)
        return render_template("error.html")
    except TypeError as e:
        logger.error(e)
        return render_template("error.html")


@app.route('/stream/<category_label>', methods=['GET'])
def stream(category_label):
    """Get entries from Feedly stream (category)."""
    try:
        # TODO: remove fixed ID and encode within config file
        id = 'ee31f0cd-2a7e-462a-9c14-4bd3a50ebfa5'
        user = get_feedly_user(id)
        category = Category.query.filter_by(label=category_label).first()
        stream_id = category.id

        oauth = FeedlyClient(user.access_token)
        resp = oauth.get_stream(stream_id, 100)
        data = json.loads(resp.data)

        entries = data.get('items')
        for entry in entries:
            ent = Entry.query.filter_by(id=entry.get('id')).first()
            if not ent:
                ent = Entry(
                    id=entry.get('id'),
                    title=entry.get('title', ""),
                    keywords=','.join(entry.get('keywords', "")),
                    published=entry.get('published', ""),
                    engagement=entry.get('engagement', ""),
                    engagementRate=entry.get('engagementRate', "")
                )
            else:
                ent.title = entry.get('title', "")
                ent.keywords = ','.join(entry.get('keywords', ""))
                ent.published = entry.get('published', "")
                ent.engagement = entry.get('engagement', "")
                ent.engagementRate = entry.get('engagementRate', "")
            ent.save()

        return resp

    except TypeError as e:
        logger.error(e)
        return render_template("error.html")
    except TypeError as e:
        logger.error(e)
        return render_template("error.html")


def get_feedly_user(user_id):
    """Get user object from feedlyclient database.

    :param user_id: User ID string (Feedly user ID).
    :return: user object.
    """
    user = User.query.filter_by(feedly_id=user_id).first()
    if user.token_expiry_date < datetime.datetime.now():
        oauth = FeedlyClientAuth()
        access_token, expiry_date = oauth.refresh_access_token(user.refresh_token)
        user.access_token = access_token
        user.token_expiry_date = expiry_date
        user.save()
    return user
