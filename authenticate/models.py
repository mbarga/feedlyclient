# -*- coding: UTF-8 -*-

from flask_login import UserMixin

from .import db, lm


#https://blog.miguelgrinberg.com/post/oauth-authentication-with-flask
class User(UserMixin, db.Document):
    #TODO collection name?
    feedly_id = db.StringField()
    email = db.StringField()
    #TODO encrypt tokens
    access_token = db.StringField()
    refresh_token = db.StringField()
    token_expiry_date = db.DateTimeField()

    @property
    def is_authenticated(self):
        return True

    @property
    def is_active(self):
        return True

    @property
    def is_anonymous(self):
        return False

    def get_id(self):
        return str(self.feedly_id)

    def __repr__(self):
        return '<User %r>' % (self.email)


@lm.user_loader
def load_user(id):
    return User.query.get(str(id))


class Category(db.Document):
    id = db.StringField()
    label = db.StringField()
    description = db.StringField


class Entry(db.Document):
    id = db.StringField()
    title = db.StringField()
    keywords = db.StringField()
    published = db.IntField()
    engagement = db.StringField()
    engagementRate = db.StringField()
    # summary = db.StringField()
    # link = db.StringField()
