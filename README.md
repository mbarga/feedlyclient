# feedlyclient
A python wrapper for the feedly Cloud API and a basic dashboard app written with React. The dashboard provides functionality to get a high level view of the content in your feeds.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine.

### Prerequisites
What things you need to install the software and how to install them

The feedly Cloud API uses OAuth 2.0 authentication. For development and testing use, the feedly team provides a sandbox environment and they publish public client ID and client secret keys for this environment every month on the following [forum](https://groups.google.com/forum/#!forum/feedly-cloud). You can find more details on the feedly [sandbox page](https://developer.feedly.com/v3/sandbox/).

You will need to create a [developer account](https://developer.feedly.com/v3/developer/) to obtain client ID and client secret keys needed for authentication on the production feedly environment.

#### Authentication
feedlyclient finds your OAuth keys from environment variables. Please make sure that the `FEEDLY_CLIENT_SECRET` and `FEEDLY_CLIENT_ID` variables are defined.
You can define them in a bash shell
 - `export FEEDLY_CLIENT_ID=<your_client_id>
 - `export FEEDLY_CLIENT_SECRET=<your_client_secret>
or in your IDE environment (e.g. Pycharm)
 - [Edit Configurations] -> Defaults -> Python -> Environment -> Environment Variables

#### Getting Started with the Wrapper Functions
<code to set up user>
<code to authenticate>
<code to get subscriptions>
